all:

deploy-workstation:
	if ! diff -q /etc/nftables.conf nftables-workstation.nft > /dev/null; \
	then \
	  sudo cp nftables-workstation.nft /etc/nftables.conf; \
	  sudo etckeeper vcs commit -m "nftables.conf: import latest upstream config" -- nftables.conf; \
	fi
	for ipt in iptables ip6tables; \
	do \
	  sudo $$ipt -F; \
	  sudo $$ipt -X; \
	  for chain in INPUT OUTPUT FORWARD;\
	  do \
	    sudo $$ipt -P $$chain ACCEPT; \
	  done \
	done
	sudo systemctl restart nftables.service
